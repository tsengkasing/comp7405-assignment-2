#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from math import log, sqrt, e
import scipy.stats as si

def black_scholes(S, K, t, T, sigma, r):
    """Calculate Black-Scholes Formulas

    Args:
        S: int, spot price.
        K: int, strike price.
        t: int, begin time.
        T: int, time to maturity.
        sigma: double, volatility of underlying asset.
        r: double, risk-free interest rate.

    Returns:
        values of both call and put options.
    """
    d1 = (log(S / K) + r * (T - t)) / (sigma * sqrt(T - t)) + 0.5 * sigma * sqrt(T - t)
    d2 = (log(S / K) + r * (T - t)) / (sigma * sqrt(T - t)) - 0.5 * sigma * sqrt(T - t)

    call_option = S * si.norm.cdf(d1, 0.0, 1.0) - K * (e ** (-r * (T - t))) * si.norm.cdf(d2, 0.0, 1.0)
    put_option = K * (e ** (-r * (T - t))) * si.norm.cdf(-d2, 0.0, 1.0) - S * si.norm.cdf(-d1, 0.0, 1.0)

    return round(call_option, 4), round(put_option, 4)

if __name__ == '__main__':
    call_option, put_option = black_scholes(S=100, K=100, t=0, T=0.5, sigma=0.2, r=0.01)
    print("S=100, K=100, t=0, T=0.5, sigma=0.2, r=0.01 => Call [{}], Put [{}]".format(call_option, put_option))
    call_option, put_option = black_scholes(S=100, K=100, t=0, T=1.0, sigma=0.2, r=0.01)
    print("S=100, K=100, t=0, T=1.0, sigma=0.2, r=0.01 => Call [{}], Put [{}]".format(call_option, put_option))
    call_option, put_option = black_scholes(S=100, K=100, t=0, T=1.5, sigma=0.2, r=0.01)
    print("S=100, K=100, t=0, T=1.5, sigma=0.2, r=0.01 => Call [{}], Put [{}]".format(call_option, put_option))
    # call_option, put_option = black_scholes(S=100, K=120, t=0, T=0.5, sigma=0.2, r=0.01)
    # print("S=100, K=120, t=0, T=0.5, sigma=0.2, r=0.01 => Call [{}], Put [{}]".format(call_option, put_option))
    # call_option, put_option = black_scholes(S=100, K=100, t=0, T=1.0, sigma=0.2, r=0.01)
    # print("S=100, K=100, t=0, T=1.0, sigma=0.2, r=0.01 => Call [{}], Put [{}]".format(call_option, put_option))
    call_option, put_option = black_scholes(S=100, K=100, t=0, T=0.5, sigma=0.3, r=0.01)
    print("S=100, K=100, t=0, T=0.5, sigma=0.3, r=0.01 => Call [{}], Put [{}]".format(call_option, put_option))
    call_option, put_option = black_scholes(S=100, K=100, t=0, T=0.5, sigma=0.2, r=0.02)
    print("S=100, K=100, t=0, T=0.5, sigma=0.2, r=0.02 => Call [{}], Put [{}]".format(call_option, put_option))
