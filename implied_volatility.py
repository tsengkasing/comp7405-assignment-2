#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import scipy.stats as si
from math import sqrt, log, e

def calc_implied_volatility(S0, K, t, T, r, C_true, q):
    sigmahat = sqrt(2 * abs((log(S0 / K) + (r - q) * (T - t) ) / (T - t)))
    tol = 1e-8
    sigma = sigmahat
    sigmadiff = 1
    n = 1
    n_max = 100
    while sigmadiff >= tol and n < n_max:
        C = price_call(S0, K, t, T, sigma, r, q)
        Cvega = vega_call(S0, K, r, sigma, T, q, t)
        increment = (C - C_true) / Cvega
        sigma -= increment
        n += 1
        sigmadiff = abs(increment)
    if n >= n_max and sigmadiff >= tol:
        return 'NaN'
    return sigma

def price_call(S, K, t, T, sigma, r, q):
    call_option, _ = black_scholes_extended(S, K, t, T, sigma, r, q)
    return call_option

def vega_call(S, K, r, sigma, T, q, t):
    d1 = (log(S / K) + (r + 0.5 * (sigma ** 2)) * T) / (sigma * sqrt(T))
    return S * (e ** (-q * (T - t))) * sqrt(T - t) * si.norm.cdf(d1, 0.0, 1.0)

def black_scholes_extended(S, K, t, T, sigma, r, q):
    d1 = (log(S / K) + (r - q) * (T - t)) / (sigma * sqrt(T - t)) + 0.5 * sigma * sqrt(T - t)
    d2 = (log(S / K) + r * (T - t)) / (sigma * sqrt(T - t)) - 0.5 * sigma * sqrt(T - t)

    call_option = S * (e ** (-q * (T - t))) * si.norm.cdf(d1, 0.0, 1.0) - K * (e ** (-r * (T - t))) * si.norm.cdf(d2, 0.0, 1.0)
    put_option = K * (e ** (-r * (T - t))) * si.norm.cdf(-d2, 0.0, 1.0) - S * (e ** (-q * (T - t))) * si.norm.cdf(-d1, 0.0, 1.0)

    return round(call_option, 4), round(put_option, 5)

if __name__ == '__main__':
    sigma = calc_implied_volatility(S0=100, K=100, t=0, T=1.0, r=0.01, C_true=1, q=1)
    print(sigma)
