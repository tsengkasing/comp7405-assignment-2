#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from math import sqrt
import numpy as np

# Using Standard Normal Random Variables Generator
# <numpy.random.normal>

# generate 200 samples of X and Y
X = np.random.normal(0.0, 1.0, 200)
Y = np.random.normal(0.0, 1.0, 200)

# Using 0.5 as correlation ρ
ρ = 0.5
# Generate the samples of Z
Z = ρ * X + sqrt(1 - ρ ** 2) * Y

# Calculate correlation coefficient
ρ_X_Z = np.cov(X, Z)[0][1] / sqrt(np.var(X) * np.var(Z))

print("ρ(X, Z) = {}, ρ = {}".format(ρ_X_Z, ρ))
