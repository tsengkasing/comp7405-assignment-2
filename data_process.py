#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import csv
from os import path
from dateutil import parser

path_instruments = path.join(path.dirname(__file__), 'data', 'instruments.csv')
path_marketdata = path.join(path.dirname(__file__), 'data', 'marketdata.csv')

instruments = {}
marketdata = {}

# Load Instruments
with open(path_instruments) as file_instruments:
    instruments_list = csv.reader(file_instruments)
    for row in instruments_list:
        asset_type, symbol, expiry, strike, option_type = row
        # Skip Title
        if asset_type == 'Type':
            continue
        instruments[symbol] = {
            'type': asset_type,
            'expiry': expiry,
            'strike': strike,
            'option_type': option_type
        }

with open(path_marketdata) as file_marketdata:
    marketdata_list = csv.reader(file_marketdata)
    for row in marketdata_list:
        if len(row) != 7:
            continue
        if row[0] == 'LocalTime':
            continue
        time, symbol, _, bid, _, ask, _ = row
        if symbol in marketdata:
            marketdata[symbol].append({
                'time': time,
                'bid': bid,
                'ask': ask
            })
        else:
            marketdata[symbol] = [{
                'time': time,
                'bid': bid,
                'ask': ask
            }]

# Merge Information
for symbol in instruments:
    if symbol in marketdata:
        instruments[symbol]['market'] = marketdata[symbol]

def dump_json():
    with open(path.join(path.dirname(__file__), 'data', 'info.json'), 'w') as file_output:
        json.dump(instruments, file_output, indent=4)

for minute in [31, 32, 33]:
    for symbol in instruments:
        market = instruments[symbol]['market']
        for i in range(0, len(market)):
            time = parser.parse(market[i]['time'])
            if time.minute == minute:
                if time.second == 0 and time.microsecond == 0:
                    instruments[symbol][minute] = market[i]
                else:
                    instruments[symbol][minute] = market[i - 1]
                break
        else:
            instruments[symbol][minute] = market[len(market) - 1]

dump_json()
